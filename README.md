# OpenML dataset: Melbourne_Housing_Snapshot

https://www.openml.org/d/46077

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

### Description:

The dataset, named `melb_data.csv`, represents detailed information about real estate sales in various suburbs across Melbourne. It captures specific characteristics of residential properties along with sales information, providing a comprehensive snapshot for potential buyers, sellers, and analysts.

### Attribute Description:

- **Suburb**: Name of the suburb where the property is located.
- **Address**: Specific address of the property.
- **Rooms**: Number of rooms in the property.
- **Type**: Type of dwelling (h: house, u: unit/duplex).
- **Price**: Sale price of the property in Australian dollars.
- **Method**: Sale method (S: Sold).
- **SellerG**: Real estate agency or agent selling the property.
- **Date**: Date of sale.
- **Distance**: Distance from the property to the Central Business District (CBD) in kilometers.
- **Postcode**: The postal code of the property location.
- **Bedroom2**: Number of bedrooms (alternative count to Rooms).
- **Bathroom**: Number of bathrooms.
- **Car**: Number of parking spaces.
- **Landsize**: Size of the land on which the property is situated in square meters.
- **BuildingArea**: Size of the building in square meters; some values are missing (noted as 'nan').
- **YearBuilt**: The year the building was constructed; some dates are missing (noted as 'nan').
- **CouncilArea**: Governed council area for the property.
- **Lattitude**: Geographic coordinate specifying the north-south position.
- **Longtitude**: Geographic coordinate specifying the east-west position.
- **Regionname**: General region (metropolitan region) where the property is located.
- **Propertycount**: Number of properties in the suburb.

### Use Case:

This dataset is predominantly geared towards property investors, real estate analysts, and market researchers who aim to understand the dynamics of Melbourne's real estate market. It can be used for various analyses, such as identifying property price trends, understanding the impact of location on property prices, and examining the features that contribute to the valuation of residential real estate. Additionally, the dataset is valuable for potential homebuyers seeking insights into property attributes and pricing within different Melbourne suburbs.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46077) of an [OpenML dataset](https://www.openml.org/d/46077). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46077/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46077/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46077/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

